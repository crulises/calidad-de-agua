# Calidad de Agua

El proyecto en este repositorio tiene como objetivo el uso y la aplicación del aprendizaje máquina a diferentes conjuntos de datos con fin de determinar diferentes cuestiones con respecto a la calidad del agua.

| Nro | Participantes | Anotaciones de Reunión  | Avances / Tareas realizadas a partir de reunión |
| --- | ------------- | ----------------------- | ----------------------------------------------- |
| 1   | Uli / Zamu    | [notas](notas/reu_1.md) | [avances](notas/articulos/sesion_1.md)          |
| 2   | Uli / Zamu    | [notas](notas/reu_2.md) | [avances](notas/articulos/sesion_2.md)          |
