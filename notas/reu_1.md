# Plan

- [ ] Relevamiento de la literatura
  > Parametros a tener en cuenta: Temperatura, Cloro, Ph
  - [ ] Identificar datasets
  - [ ] Identificar técnicas
- [ ] ... TBD ...
- [ ] Replicar experimento

# A tener en cuenta

- Verlo en una perspectiva de costos, por ej: no es lo mismo medir la conductividad, que medir alguna variable que necesite algún reactivo importado, poder estimar estos parámetros a partir de otros parámetros con cierto nivel de confianza
