# Parametros fisicoquimicos
> Fuente: [Water quality assesment](https://link.springer.com/article/10.1007/s40899-015-0014-7)
Estos son importantes porque el agua contiene diferentes impuridades biológicas, como tambien es necesario llevar a cabo diferentes pruebas que permitan checkear cuestiones fisicas del agua. 

- Temperatura: Esta será una de las variables primordiales a la hora de la conservación de la vida de los microorganismos en el agua, como así tambien de la fauna marina que se pueda encontrar. Esta puede afectar el nivel de oxigeno en el agua.
- pH: Actividad fotosintética reducida como así también la asimilación de CO2 están fuertemente ligadas a altos niveles de pH. Y bajos niveles de oxigeno se pueden relacionar con altas temparaturas en los meses de verano.
- EC (Electrical conductivity)
- BOD (Biochemical Oxigen Demand)
- Sulfato
- Calcio
- Magnesio
- Sodio y potasio
- Fósforo
- Nitrato y amonio
- TDS y TSS (Sólidos que pueden ser encontrados en las corrientes en tres formas suspendidos, volatiles y disueltos)

## Datasets

### Kaggle
- Si bien este dataset tiene varias de las metricas que estamos buscando y además está bastante "limpio", esto no parece ser algo real, parece ser un dataset generado.
    - La generación de nuestro dataset (teniendo en cuenta diferentes estadísticas) es factible?
- Voy a seguir investigando la factibilidad de los muchos datasets que tenemos en Kaggle, a ver si alguno sirve. Sin embargo, creo que todos tienen el mismo origen, y no se podrá extraer algun modelo de regresión que asocie de manera acertada a unas variables con otras
- Ref: https://www.kaggle.com/adityakadiwal/water-potability 

### Danubio
- Me encontré con que donde se tome esta medición hará que varíe mucho el resultado, además en uno de los estudios estos 
- Si bien las mediciones de un lugar a otro varían muchísimo, se puede encontrar alguna correlación que se presente en todos los ambientes, por ejemplo, la relación entre temperatura y cantidad de oxigeno en el agua
    - Opciones. Escribirles a diferentes autores para solicitarles su dataset.
- Ref: https://sci-hub.se/10.1007/s11356-021-15135-3

### Taiwan
- Este genera su dataset a partir de cosas que no tienen que ver con el parametro propiamente dicho, sino que de cuestiones externas a lo fisico-quimico
- Este tiene en cuenta factores hidrologicos (descarga, días sin descarga, temperatura del agua, lluvia) para predecir la concentración de nitrogeno de amonio (NH3-N)
- Ref: https://www.sciencedirect.com/science/article/pii/S030147971400591X?via%3Dihub

### Almeida RWQI
- Este es un estudio llevado a cabo en Argentina que tiene como objetivo la salud de los nadadores, la R es por Recreacional.
- Sin embargo, este tiene varios de los indicadores que nos interesan, por lo que el dataset puede llegar a servir
- Ref: https://link.springer.com/article/10.1007/s11356-012-0865-5

### Turquia
- Este estudia el contenido de metales pesados en muestras de agua en un rio en Turquia
- Busca establecer alguna correlación entre diferentes sucesos y cambios en el indice de calidad del agua.
- Ref: https://link.springer.com/article/10.1007/s11356-021-15135-3

## Data.world

> Seguir explorando otros conjuntos de datos acá: https://data.world/datasets/water-quality

- Austin texas water quality: https://data.austintexas.gov/Environment/Water-Quality-Sampling-Data/5tye-7ray
- Winipeg water quality: https://data.world/canwin/city-of-winnipeg-rivers-and-streams-data
- UK water quality: https://data.world/datagov-uk/0d1f2a64-6ca4-4da2-943e-94fdc452e4e3