# Consignas/Actividades

- Orientar el desarrollo de este trabajo hacia los enfoques del aprendizaje máquina.
- Dedicar el tiempo hasta la próxima reunión recolectando/estudiando conjuntos
  de datos que se puedan utilizar para los diferentes acercamientos de ML.
- A medida que se va avanzando, revisar que preguntas de negocio pueden ser
  respondidas con los datos que se van teniendo

# Plan

- [ ] Revisión de literatura
  > Teniendo énfasis en la aplicación del Aprendizaje Máquina a las variables
  > de calidad de agua.
- [ ] Buscar/Estudiar datasets en diferentes fuentes para lograr obtener un
      conjunto de datos más "potables" como para avanzar con la creación de un
      modelo de ML.
- [ ] Formular preguntas tentativas que se consideren de interés para el
      negocio desde el punto de vista subjetivo
  > Esto por el hecho de que no se cuenta con ningún experto a disposición como
  > para realizar este trabajo con mas detenimiento

# A tener en cuenta

- Eduardo Zamudio, va a crear un documento con algunas de las preguntas que él
  confeccionó a lo largo del proyecto. Estas preguntas servirán de referencia
  a la hora de tener en claro un objetivo al que apuntar (siempre teniendo en
  cuenta el enfoque del aprendizaje máquina).
